package lessons;

public class Interface {
    /*
        Define an interface, which contains the signature of the functions.

        Explanation:
        -> Signature consists of function name, parameters and return type
    */
    interface Dialogs {
        void hi(String name);

        void bye(String name);
    }

    /*
        A class can implements one or multiple interfaces.
     */
    static class NPC implements Dialogs {

        @Override
        public void hi(String name) {
            System.out.println("Hi " + name);
        }

        @Override
        public void bye(String name) {
            System.out.println("Bye " + name);
        }
    }

    /*
        Interface can be passed as parameter to a function.
        The function caller needs to implement the functions before use.
     */
    public static void doSomething(Dialogs dialogs) {
        dialogs.hi("Alice");
        dialogs.bye("Bob");
    }

    /*
        Interface can be inherited, which improve the isomorphism.
     */
    interface GreetingsTypeA extends Dialogs {
        @Override
        default void hi(String name) {
            System.out.println("GreetingsTypeA Hi " + name);
        }

        @Override
        default void bye(String name) {
            System.out.println("GreetingsTypeA Bye " + name);
        }
    }

    interface GreetingsTypeB extends Dialogs {
    }

    /*
        In Android app development, interface will be used as callback function.
        It will be passed to the function, which is like the doSomething function.

        Explanation:
        --> Since we cannot predict when the button will be clicked by the user,
        therefore, we need to pass the implementation to the function (interface).
     */
    interface OnButtonClickListener {
        void click();
    }

    public static void main(String[] args) {
        NPC npc = new NPC();
        npc.hi("Alice");
        npc.hi("Bob");

        doSomething(new Dialogs() {
            @Override
            public void hi(String name) {
                System.out.println("Hi " + name);
            }

            @Override
            public void bye(String name) {
                System.out.println("Bye " + name);
            }
        });

/*
        Since the GreetingsTypeA & GreetingsTypeB are inherited from Dialogs,
        we can pass the implementation of GreetingsTypeA / GreetingsTypeB to
        function doSomething directly.
 */
        doSomething(new GreetingsTypeA() {
            @Override
            public void hi(String name) {
                GreetingsTypeA.super.hi(name);
            }

            @Override
            public void bye(String name) {
                GreetingsTypeA.super.bye(name);
            }
        });

        doSomething(new GreetingsTypeB() {
            @Override
            public void hi(String name) {
                System.out.println("GreetingsTypeB Hi " + name);
            }

            @Override
            public void bye(String name) {
                System.out.println("GreetingsTypeB Bye " + name);
            }
        });

//        You may define the implementation of the interface as a variable
        Dialogs implDialogs = new Dialogs() {
            @Override
            public void hi(String name) {
                System.out.println("implDialogs Hi " + name);
            }

            @Override
            public void bye(String name) {
                System.out.println("implDialogs Bye " + name);
            }
        };
        doSomething(implDialogs);
    }

}