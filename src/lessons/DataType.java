package lessons;

/*
    Reference:
    1. https://www.geeksforgeeks.org/java-lang-integer-class-java/
    2. https://www.tutorialspoint.com/java/java_basic_datatypes.htm
 */
public class DataType {
    public static void run() {
/*
    Primitive types

    Primitive types are the most basic data types available within the Java language. There are 8: boolean, byte,
    char, short, int, long, float and double. These types serve as the building blocks of data manipulation in Java.
    Such types serve only one purpose — containing pure, simple values of a kind. Because these data types are defined
    into the Java type system by default, they come with a number of operations predefined. You can not define a new
    operation for such primitive types.
 */
        boolean pribool;
        char priChar;
        byte priByte;
        short priShort;
        int priInt;
        long priLong;
        float priFloat;
        double priDouble;

/*
    Wrapper classes
    Integer class is a wrapper class for the primitive type int which contains several methods to effectively deal
    with a int value like converting it to a string representation, and vice-versa. An object of Integer class can
    hold a single int value.
 */
        Boolean wrapBool;
        Character wrapChar;
        Byte wrapByte;
        Short wrapShort;
        Integer wrapInt;
        Long wrapLong;
        Float wrapFloat;
        Double wrapDouble;

/*
    Manipulate the data

    The above code defined 16 variables, the syntax is as follow:
    --> [ Data type / Class ] variableName;

    Explanation: Data type or class name + variable name + ; (semicolon)

    Here are the examples of assigning the value to the variables.
 */
//        Boolean / boolean (true / false)
        pribool = true;
        pribool = false;
        wrapBool = true;
        wrapBool = false;

//        Character / char (Single alphabet character)
//        IMPORTANT: Please use single quote !!!
        priChar = 'A';
        wrapChar = 'A';

//        Byte / byte (8-bit signed two's complement integer, min is -128 & max is 127)
//        No need to understand at the beginning.
        priByte = 127;
        wrapByte = -128;
        priByte = Byte.MAX_VALUE;
        wrapByte = Byte.MIN_VALUE;

//        Short / short (16-bit signed two's complement integer, min is -32,768 (-2^15) & max is 32,767 (2^15 - 1))
//        No need to understand at the beginning.
        priShort = 32767;
        wrapShort = -32768;
        priShort = Short.MAX_VALUE;
        wrapShort = Short.MIN_VALUE;

//        Integer / int (32-bit signed two's complement integer, min is 2,147,483,648 (-2^31) & max is 2,147,483,647 (2^31 -1))
//        You may consider it is a data type for storing the integers.
        priInt = 2147483647;
        wrapInt = -2147483648;
        priInt = Integer.MAX_VALUE;
        wrapInt = Integer.MIN_VALUE;

//        Long / long (64-bit signed two's complement integer, min is -9,223,372,036,854,775,808(-2^63) & max is 9,223,372,036,854,775,807 (2^63 -1))
//        You may consider it is a data type for storing the larger integers.
//        IMPORTANT: Add a 'l' or 'L' at the end to indicate that it is a long type value.
        priLong = 9223372036854775807l;
        wrapLong = -9223372036854775808L;
        priLong = Long.MAX_VALUE;
        wrapLong = Long.MIN_VALUE;

//        Float / float (single-precision 32-bit IEEE 754 floating point)
//        You may consider it is a data type for storing the numeric data with decimal points.
//        It is more accurate in storing the numbers.
//        IMPORTANT: Add a 'f' or 'F' at the end to indicate that it is a float type value.
        priFloat = 1.24f;
        wrapFloat = 1.24F;
        priFloat = Float.MAX_VALUE;
        wrapFloat = Float.MIN_VALUE;

//        Double / double (double-precision 64-bit IEEE 754 floating point)
//        It should never be used for precise values such as currency
//        The character 'd' or 'D' is not necessary.
        priDouble = 1.24;
        wrapDouble = 1.24;
        priDouble = 1.24d;
        wrapDouble = 1.24d;
        priDouble = Double.MAX_VALUE;
        wrapDouble = Double.MIN_VALUE;
    }
}
